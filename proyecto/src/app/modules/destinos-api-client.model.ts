import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './DestinoViaje.model';

export class DestinosApiClient{
	
	destinos: DestinoViaje[];
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

	constructor(){
		this.destinos = []
	}

	add(destino: DestinoViaje){
		this.destinos.push(destino);
	}

	getAll():DestinoViaje[]{
		return this.destinos;
	}
	
	elegido(destino: DestinoViaje){
    this.destinos.forEach(x=> x.setSelected(false));
		destino.setSelected(true);
		this.current.next(destino);
	}

	subscribeOnChange(fn){
		this.current.subscribe(fn);
	}

	getById(id: String): DestinoViaje{
		console.log('llamada');
		return null;
	}
	
}
