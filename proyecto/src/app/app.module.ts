import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrincipalComponent } from './Componentes/principal/principal.component';
import { CabeceroComponent } from './Componentes/cabecero/cabecero.component';
import { PieDePaginaComponent } from './Componentes/pie-de-pagina/pie-de-pagina.component';
import { ListadoComponent } from './Componentes/listado/listado.component';
import { DestinosViajesComponent } from './Componentes/destinos-viajes/destinos-viajes.component';
import { DestinoDetalleComponent } from './Componentes/destino-detalle/destino-detalle.component';
import { FormDestinoComponent } from './Componentes/form-destino/form-destino.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { VuelosComponent } from './Componentes/vuelos/vuelos.component';
import { ReservasModule } from './reservas/reservas.module';
import { LoginComponent } from './Componentes/login/login.component';
import { ProtectedComponent } from './Componentes/protected/protected.component';



const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: PrincipalComponent},
  { path: 'destino', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate:[ UsuarioLogueadoGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    CabeceroComponent,
    PieDePaginaComponent,
    ListadoComponent,
    DestinosViajesComponent,
    DestinoDetalleComponent,
    FormDestinoComponent,
    VuelosComponent,
    LoginComponent,
    ProtectedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    ReservasModule
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
