
import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from 'src/app/modules/DestinoViaje.model';


@Component({
  selector: 'app-destinos-viajes',
  templateUrl: './destinos-viajes.component.html',
  styleUrls: ['./destinos-viajes.component.css']
})
export class DestinosViajesComponent implements OnInit {
  
  @Input() destino: DestinoViaje;
  @HostBinding('attr.class') cssClass ='col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  
  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }
  
  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

}
