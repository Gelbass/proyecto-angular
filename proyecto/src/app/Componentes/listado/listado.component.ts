import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from 'src/app/modules/DestinoViaje.model';
import { DestinosApiClient } from './../../modules/destinos-api-client.model';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css'],
  providers: [DestinosApiClient]
})
export class ListadoComponent implements OnInit {
  
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  constructor(public destinosApiCliente: DestinosApiClient ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    
   }

  ngOnInit(): void {
  }
  
  agregado(destino: DestinoViaje){
    this.destinosApiCliente.add(destino);
    this.onItemAdded.emit(destino);
    console.log(destino);
  }
  
  elegido(destino: DestinoViaje){
    this.destinosApiCliente.elegido(destino);
  }
  
 
}
