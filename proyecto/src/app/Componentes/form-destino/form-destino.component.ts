import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { DestinoViaje } from 'src/app/modules/DestinoViaje.model';

@Component({
  selector: 'app-form-destino',
  templateUrl: './form-destino.component.html',
  styleUrls: ['./form-destino.component.css']
})
export class FormDestinoComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLong = 3

  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['',Validators.compose([
        Validators.required,
        this.validarNombre,
        this.validarNombreParametrizable(this.minLong)
      ])],
      url: ['']
    });
  }

  ngOnInit(): void {
  }
  guardarDestinoViaje(nombre:string,url:string){
    const destinos = new DestinoViaje(nombre,url);
    this.onItemAdded.emit(destinos);
    console.log(destinos);
    return false;
  }

  validarNombre(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidNombre: true };
    } 
    return null;
  }

  validarNombreParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[ s: string ]: boolean} | null=>{
      const l = control.value.toString().trim().length;
      console.log(l);
    if (l > 0 && l < minLong) {
      return { minLongNombre: true}
      }
      return null;
    }
  }

}
