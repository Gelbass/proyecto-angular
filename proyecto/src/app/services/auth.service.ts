import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(user: string, pass: string): boolean{ 
    if (user === 'user' && pass === 'pass') {
      localStorage.setItem('usuario',user);
      return true;
    };
    return false;
  }

  logout():any{
    localStorage.removeItem('user');
  }

  getUser():any{
    return localStorage.getItem('user');
  }

  isLoggeedIn():boolean{
    return this.getUser()!== null;
  }
}
